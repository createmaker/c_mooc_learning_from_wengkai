
#include <stdio.h>
//double max(double a,double b);
/**
 * #include有两种形式来指出要插入的文件
 * ""要求编译器首先在当前目录（.c文件所在的目录）寻找这个文件，
 * 如果没有，到编译器指定的目录去找
 * <>让编译器只在指定的目录去找
 * 编译器自己知道自己的标准库的头文件在哪里
 * 环境变量和编译器命令行参数也可以指定寻找头文件的目录
 */
#include "max.h"
int main(void){
    int a = 5;
    int b = 6;
    printf("%f\n",max(a,b));
    return 0;
}